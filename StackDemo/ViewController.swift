//
//  ViewController.swift
//  StackDemo
//
//  Created by Josh Campion on 06/01/2016.
//  Copyright © 2016 Josh Campion. All rights reserved.
//

import UIKit
import StackView

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let ss = StringsStack(strings: ["Hello", "World"])
        
        var stack = ss.stack
        stack.stackAlignment = .Center
        stack.stackDistribution = .FillEqually
        stack.axis = .Vertical
        
        let logIn = LogInStack()
        
        var container = CreateStackView([logIn.stackView, ss.stackView])
        
        container.axis = .Vertical
        container.spacing = 32.0
        
        container.view.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(container.view)
        view.addConstraints(NSLayoutConstraint.constraintsToAlign(view: container.view, to: view, withInsets: UIEdgeInsetsMake(32, 32, 32, 32)))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class LogInStack: CreatedStack {
    
    let emailStack = ErrorTextFieldStack()
    let passwordStack = ErrorTextFieldStack()
    
    init() {
        super.init(arrangedSubviews: [emailStack.stackView, passwordStack.stackView])
        
        emailStack.validation = EmailValidationWithMessage("Please enter a valid email.")
        emailStack.placeholderText = "Email Address"
        
        passwordStack.validation = NullStringValidation("Please enter a password.")
        passwordStack.placeholderText = "Password"
        
        emailStack.textField.keyboardType = UIKeyboardType.EmailAddress
        emailStack.textField.autocorrectionType = .No
        
        passwordStack.textField.secureTextEntry = true
        passwordStack.textField.autocorrectionType = .No
        
        stack.axis = .Vertical
        stack.spacing = 8.0
    }
    
}